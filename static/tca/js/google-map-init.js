jQuery(document).ready(function() {
    "use strict";


    function b() {
        var a = {
                zoom: 10,
                scrollwheel: false,
                center: new google.maps.LatLng(55.76585256897282, 37.6335175),
                styles: [
                    {
                        "featureType": "road",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.province",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "stylers": [
                            {
                                "color": "#162d4d"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape.natural",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.country",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#f5f5f5"
                            },
                            {
                                "lightness": 10
                            },
                            {
                                "weight": 1
                            }
                        ]
                    }
                ]
            },
            b = document.getElementById("map"),
            c = new google.maps.Map(b, a);
        new google.maps.Marker({
            position: new google.maps.LatLng(55.76585256897282, 37.6335175),
            map: c,
            title: "TCA"
        })
    }
    google.maps.event.addDomListener(window, "load", b);

});