#coding: utf-8
from django.core.exceptions import ObjectDoesNotExist
from django.template import Library
from django.utils.safestring import mark_safe
from contacts.models import Contact, Site

register = Library()

@register.simple_tag(takes_context=True)
def tca_contacts(context):
    try:
        contacts = Contact.objects.get(site=Site.TCA)
    except ObjectDoesNotExist:
        contacts = {}

    return contacts


@register.simple_tag(takes_context=True)
def audit_contacts(context):
    try:
        contacts = Contact.objects.get(site=Site.AUDIT)
    except ObjectDoesNotExist:
        contacts = {}

    return contacts