#coding: utf-8
from django.db import models


class Site(object):
    """
    """
    TCA = 1
    AUDIT = 2

    values = {
        TCA: 'ТСА',
        AUDIT: 'Аудит'
    }

    @classmethod
    def get_choices(cls):
        return [
            (cls.TCA, 'ТСА'),
            (cls.AUDIT, 'Аудит')
        ]



class Contact(models.Model):
    """

    """
    site = models.PositiveIntegerField(verbose_name='Сайт',
                                       choices=Site.get_choices())
    address = models.TextField(verbose_name='Адрес', null=True, blank=True)
    postal_address = models.TextField(verbose_name='Почтовый адрес', null=True, blank=True)
    phone = models.TextField(verbose_name='Телефон', null=True, blank=True)
    email = models.TextField(verbose_name='email', null=True, blank=True)

    class Meta:
        verbose_name = 'Контакты'
        verbose_name_plural = 'Контакты'

    def __str__(self):
        return "%s" % Site.values[self.site]

